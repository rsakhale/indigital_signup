-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 30, 2017 at 03:42 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blogger`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_size` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `company_name`, `designation`, `company_size`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Aakesh', 'aakesh@gmail.com', '$2y$10$s.SLFz3obquU2iULc.NNs.OieJyKB1Q9fIuuT6DgO4MS.ld3.e9pO', '', '', 0, 'qe6a4RTLu3u3pJumTRGn3EJVOMEc45rHhZVQ32u6Zcorz6XVxFQrqE3a8tDc', '2017-08-14 03:15:30', '2017-08-14 03:15:30'),
(2, 'Aakesh', 'asdasd@sadf.com', '$2y$10$Dbz1dyE545/o3OJUrTtIoee7ETl1VWheKfJW86ODFmwE2bysxBHxC', '', '', 0, 'Q8KJuCMyW7wy23yIA4viYZ4U1F7LuJy4WQpQjHXLN4324ne05i5YyCskZb4t', '2017-08-30 06:45:22', '2017-08-30 06:45:22'),
(3, 'asdsdasd', 'asd@gmail.com', '$2y$10$06yRIyFQihmPZLCbD8ZOyuvJ3gSCVN0GvrBPk11aK/1OTYnb2ja32', '', '', 0, 'GtZjzxukYAQesaLbgfnypwFmR8WRO7yns0dsu9eoVwRSmb9D75WSbo95OY7l', '2017-08-30 06:49:42', '2017-08-30 06:49:42'),
(4, 'tetete', 'tetete@gmail.com', '$2y$10$ron2KqAMUQpB.WmuMnFv7e0YipbvJwG3FQtMV.LN7erOWvhrI5VJC', 'testcomp', 'sdfsdf', 40, 'jYy4Bt37Ujz5Twznhz81N1N8kXOcMbt9SfarYgiV6kQiMc5gVArFDCiUBWvP', '2017-08-30 07:46:00', '2017-08-30 07:46:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
